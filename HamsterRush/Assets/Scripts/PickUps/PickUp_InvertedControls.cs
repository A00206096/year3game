﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp_InvertedControls : PickUpBasic {

    private PlayerController player;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public override void Activate(Collider other)
    {
        base.Activate(other);

        player = other.gameObject.GetComponent<PlayerController>();

        player.Inverted_controls = -1;
    }

    private void OnDestroy()
    {
        if (player != null)
        {
            player.Inverted_controls = 1;
        }
        
    }

}
