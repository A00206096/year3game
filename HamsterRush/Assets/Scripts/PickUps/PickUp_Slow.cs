﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp_Slow : PickUpBasic
{

    [Range(1.0f, 5.0f)]
    public float slow_speed_factor = 0.5f;

    private PlayerController player;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public override void Activate(Collider other)
    {
        base.Activate(other);

        player = other.gameObject.GetComponent<PlayerController>();

        player.SuperSpeed_noJump_factor = this.slow_speed_factor;
    }

    private void OnDestroy()
    {
        if (player != null)
        {
            player.SuperSpeed_noJump_factor = 1;
        }

    }

}
