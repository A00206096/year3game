﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpBasic : MonoBehaviour {

    public float duration = 1.0f;
    protected bool activated = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    void OnTriggerEnter(Collider other)
    {
        Activate(other);
    }

    public virtual void Activate(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            this.gameObject.GetComponent<Renderer>().enabled = false;
            activated = true;
            Destroy(this.gameObject, duration);
        }

    }
    
}
