﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp_SuperSpeed_NoJump : PickUpBasic {

    [Range(1.0f, 5.0f)]
    public float super_speed_factor = 1.5f;

    private PlayerController player;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public override void Activate(Collider other)
    {
        base.Activate(other);

        player = other.gameObject.GetComponent<PlayerController>();

        player.SuperSpeed_noJump_factor = this.super_speed_factor;
        player.Can_jump = false;
    }

    private void OnDestroy()
    {
        if (player != null)
        {
            player.SuperSpeed_noJump_factor = 1;
            player.Can_jump = true; //@FIX If the player is falling and the power up runs out he will be able to jump in the air
        }
        
    }

}
