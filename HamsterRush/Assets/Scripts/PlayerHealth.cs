﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour {

    #region Public Attributes
    public const int max_health = 3;
    public const float time_damaged = 2.0f;
    #endregion

    #region Properties
    public bool Is_damaged
    {
        get { return is_damaged; }
        set { is_damaged = value; }
    }
    #endregion

    #region Private Attributes
    private int health;

    private bool is_damaged = false;
    private float current_time_damaged = 0.0f;

    private bool shield = false;
    #endregion

    #region Unity Methods
    void Start()
    {
        health = max_health;
    }

    void FixedUpdate()
    {
        

        check_damaged();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            if (!is_damaged) take_damage();
        }
    }
    
    #endregion

    #region Methods
    
    private void take_damage()
    {
        if (!shield)
        {
            health--;
        }
        else
        {
            shield = false;
        }
        is_damaged = true;
        if (health <= 0)
        {
            player_death();
        }
        //@TODO Maybe add some slowing or knockback?
    }

    private void check_damaged()
    {
        if (is_damaged)
        {
            if (current_time_damaged < time_damaged)
            {
                current_time_damaged += Time.deltaTime;
            }
            else
            {
                is_damaged = false;
                current_time_damaged = 0.0f;
            }
        }
    }
    private void player_death()
    {
        //@TODO Fade out and restart scene or respawn at checkpoint
        Debug.Log("YOU ARE DEAD");
        health = max_health;

    }
    #endregion
}
