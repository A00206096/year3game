﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEffects : MonoBehaviour {
    
    private float time_counter_toggle = 0.0f;
    private bool toggle = false;
    private PlayerHealth health_controller;
    private PlayerController player_controller;
    private Material initial_mat;
    private Color initial_color;
    private Color current_color;

    private Color damaged_color = Color.red;
    private Color inverted_color = new Color(0, 1, 1);
    private Color superSpeed_noJump_color = Color.yellow;

    // Use this for initialization
    void Start ()
    {
        health_controller = GetComponent<PlayerHealth>();
        player_controller = GetComponent<PlayerController>();
        initial_mat = GetComponent<Renderer>().material;
        initial_color = initial_mat.color;
        current_color = initial_mat.color;

    }
	
	// Update is called once per frame
	void Update ()
    {
        initial_mat.color = current_color;

        damaged_effect();
        superSpeed_noJump_effect();
        //inverted_effect();
	}


    private void damaged_effect()
    {
        if (health_controller.Is_damaged)
        {
            toggle = toggle_after_time(toggle, 0.1f);
            if (toggle)
            {
                initial_mat.color = damaged_color;
            }
            else
            {
                initial_mat.color = current_color;
            }
        }
    }

    private void superSpeed_noJump_effect()
    {
        if (player_controller.SuperSpeed_noJump_factor > 1)
        {
            current_color = superSpeed_noJump_color;
        }
        if (player_controller.SuperSpeed_noJump_factor == 1)
        {
            current_color = initial_color;
        }
    }

    private void inverted_effect()
    {
        if (player_controller.Inverted_controls == -1)
        {
            current_color = inverted_color;
        }
        if (player_controller.Inverted_controls == 1)
        {
            current_color = initial_color;
        }
    }

    private bool toggle_after_time(bool b, float sec)
    {
        if(time_counter_toggle > sec)
        {
            b = !b;
            time_counter_toggle = 0.0f;
        }
        else
        {
            time_counter_toggle += Time.deltaTime;
        }
        return b;
    }
}
