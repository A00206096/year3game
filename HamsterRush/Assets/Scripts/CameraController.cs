﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{

    protected Transform Camera;
    protected Transform Parent;

    protected Vector3 local_rotation;

    public float MouseSensitivity = 4f;
    public float OrbitDampening = 10f;

    public bool CameraDisabled = false;


    // Use this for initialization
    void Start()
    {
        this.Camera = this.transform;
        this.Parent = this.transform.parent;
    }


    void LateUpdate()
    {

        /*if (Input.GetKeyDown(KeyCode.LeftShift))
            CameraDisabled = !CameraDisabled;*/

        if (!CameraDisabled)
        {
            //Rotation of the Camera based on Mouse Coordinates
            if (Input.GetAxis("Mouse X") != 0 || Input.GetAxis("Mouse Y") != 0)
            {
                local_rotation.x += Input.GetAxis("Mouse X") * MouseSensitivity;
                local_rotation.y -= Input.GetAxis("Mouse Y") * MouseSensitivity;

                float min_rot = 10.0f;
                float max_rot = 90.0f;
                //Clamp the y Rotation to horizon and not flipping over at the top
                if (local_rotation.y < min_rot)
                    local_rotation.y = min_rot;
                else if (local_rotation.y > max_rot)
                    local_rotation.y = max_rot;
            }
        }

        //Actual Camera Rig Transformations
        Quaternion QT = Quaternion.Euler(local_rotation.y, local_rotation.x, 0);
        this.Parent.rotation = Quaternion.Lerp(this.Parent.rotation, QT, Time.deltaTime * OrbitDampening);
        
    }
}