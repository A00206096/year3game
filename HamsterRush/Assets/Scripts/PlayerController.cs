﻿using UnityEngine;

// Include the namespace required to use Unity UI
using UnityEngine.UI;

using System.Collections;

public class PlayerController : MonoBehaviour {

    #region Public Attributes
    public Transform camera_transform;
    public float speed = 600.0f;
    public float jump_height = 500.0f;
    public float bounce_height = 500.0f;
    public float k_Friction = 200.0f;
    public float gravity = 100.0f;
    public int score;
    public Text scoreText;
    //public const int max_stamina = 100;
    #endregion

    #region Properties
    public int Inverted_controls
    {
        get { return inverted_controls; }
        set
        {
            if (value == 1 || value == -1)
            {
                inverted_controls = value;
            }
            else
            {
                inverted_controls = 1;
            }
        }
    }
    public float SuperSpeed_noJump_factor
    {
        get { return superSpeed_noJump_factor; }
        set
        {
            if (value >= 1.0f)
            {
                superSpeed_noJump_factor = value;
            }
            else
            {
                superSpeed_noJump_factor = 1.0f;
            }
        }
    }
    public bool Can_jump //Used in PickUp_SuperSpeed_NoJump.cs
    {
        get { return can_jump; }
        set { can_jump = value; }
    }
    #endregion

    #region Private Attributes
    private Rigidbody rb;
    private bool can_jump = false;
    private bool is_bouncing = false;
    public int inverted_controls = 1;
    public float superSpeed_noJump_factor = 1.0f;
    //private int stamina = 100;
    #endregion

    #region Unity Methods
    void Start()
    {     
        rb = GetComponent<Rigidbody>();
        if (camera_transform == null)
        {
            camera_transform = Camera.main.transform;
        }
        score = 0;
        SetScoreText();
    }

    void FixedUpdate()
    {
        float dt = Time.deltaTime;

        full_movement(dt);

    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            can_jump = true;
            is_bouncing = false;
        }
        if (collision.gameObject.CompareTag("BounceSurface"))
        {
            bounce(Time.deltaTime);
            is_bouncing = true;
        }
    }
    #endregion

    #region Methods
    private void full_movement(float dt)
    {
        float axis_horizontal = Input.GetAxis("Horizontal") * inverted_controls;
        float axis_vertical = Input.GetAxis("Vertical") * inverted_controls;

        Vector3 movement_forward = camera_transform.forward;
        Vector3 movement_right = camera_transform.right;
        Vector3 movement_up = new Vector3(0.0f, -1.0f, 0.0f);

        movement_forward.y = 0.0f;
        movement_right.y = 0.0f;

        float speed_factors = superSpeed_noJump_factor; //We should multiply the other factors here

        movement_forward *= axis_vertical * speed * dt * speed_factors;
        movement_right *= axis_horizontal * speed * dt * speed_factors;
        movement_up *= jump(dt);

        Vector3 movement = movement_forward + movement_right + movement_up;
        Vector3 friction = rb.velocity.normalized * -k_Friction * dt;

        rb.AddForce(movement);
        rb.AddForce(friction);
    }

    private float jump(float dt)
    {
        bool key_jump = Input.GetKeyDown(KeyCode.Space);

        bool key_more_gravity = Input.GetKey(KeyCode.Space);


        float gravFactor = 0.25f;
        float more_gravity = gravity * gravFactor;
        if (key_more_gravity && rb.velocity.y > 0 && !is_bouncing)
        {
            more_gravity = 0.0f;
        }


        float ySpeed = 0.0f;
        if (key_jump && can_jump)
        {
            ySpeed = jump_height;
            can_jump = false;
        }

        Vector3 jump = new Vector3(0.0f, ySpeed * dt, 0.0f);
        rb.AddForce(jump, ForceMode.VelocityChange);

        return more_gravity;
    }

    private void bounce(float dt)
    {

        float ySpeed = bounce_height;

        Vector3 bounce = new Vector3(0.0f, ySpeed * dt, 0.0f);
        rb.AddForce(bounce, ForceMode.VelocityChange);
    }

    void SetScoreText()
    {
        scoreText.text = "Score: " + score.ToString();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            score += 10;
            SetScoreText();
        }
    }
    #endregion
}